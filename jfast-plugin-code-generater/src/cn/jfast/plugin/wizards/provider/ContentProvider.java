package cn.jfast.plugin.wizards.provider;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import cn.jfast.plugin.db.Table;

public class ContentProvider implements IStructuredContentProvider {

    @SuppressWarnings("unchecked")
	public Object[] getElements(Object inputElement) {
           if (inputElement instanceof List) {
                  return ((List<Table>)inputElement).toArray();
           } else if(inputElement.getClass().isArray()){
        	   return (Object[]) inputElement;
           }
           return new Object[0];
    }

    public void dispose() {

    }

	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
		
	}

}

