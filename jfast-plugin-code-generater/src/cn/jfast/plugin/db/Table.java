package cn.jfast.plugin.db;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cn.jfast.plugin.util.StringUtils;

public class Table {
    /** 实际表名 */
    private String tableName;
    /** 表名对应对象名小写 */
    private String nickName;
    /** 表类型 */
    private String type;
    /** 说明 */
    private String remarks;
    /** 表列 */
    private Map<String,Column> column = new HashMap<String,Column>();
    /** 表主键 */
    private Set<String> primaryKey = new HashSet<String>();
    /** 自增键 */
    private Set<String> incrementKey = new HashSet<String>();
    
    private String primaryKeyTemplate = "";
    
    private String primaryValueTemplate = "";
    
    private String primaryParamTemplate = "";
    
    private String primaryPathRouteTemplate = "";

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Column> getColumn() {
        return column;
    }

    public void setColumn(Map<String, Column> column) {
        this.column = column;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void addColumn(Column column){
        this.column.put(StringUtils.dbColumn2ModelColumn(column.getName()),column);
    }

    public void addPrimaryKey(String column){
        this.primaryKey.add(column);
        String[] keys = getPrimaryKey();
        this.primaryKeyTemplate = "";
        this.primaryParamTemplate = "";
        this.primaryValueTemplate = "";
        for(String key:keys){
        	this.primaryKeyTemplate  += StringUtils.firstCharToUpperCase(key);
        	this.primaryParamTemplate += ", "+getColumn(key).getJavaType()+" "+key;
        	this.primaryValueTemplate += ", "+key;
        	this.primaryPathRouteTemplate += "/:"+key;
        }
        this.primaryParamTemplate = this.primaryParamTemplate.substring(2);
        this.primaryValueTemplate = this.primaryValueTemplate.substring(2);
    }

    public String getPrimaryPathRouteTemplate() {
		return primaryPathRouteTemplate;
	}

	public void setPrimaryPathRouteTemplate(String primaryPathRouteTemplate) {
		this.primaryPathRouteTemplate = primaryPathRouteTemplate;
	}

	public String getPrimaryValueTemplate() {
		return primaryValueTemplate;
	}

	public void setPrimaryValueTemplate(String primaryValueTemplate) {
		this.primaryValueTemplate = primaryValueTemplate;
	}

	public Column getColumn(String columnName){
        return this.column.get(columnName);
    }

    public String[] getPrimaryKey(){
        return this.primaryKey.toArray(new String[]{});
    }
    
    public String[] getAutoIncrementKey(){
    	for(String primary:this.getPrimaryKey()){
    		if(getColumn(primary).getAutoIncrement().toLowerCase().equals("yes")){
    			incrementKey.add(primary);
    		}
    	}
    	if(incrementKey.size() > 1)
    		throw new RuntimeException("jfast 只支持单个自增字段 ");
    	return incrementKey.toArray(new String[]{});
    }
    
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

	public String getPrimaryKeyTemplate() {
		return primaryKeyTemplate;
	}

	public void setPrimaryKeyTemplate(String primaryKeyTemplate) {
		this.primaryKeyTemplate = primaryKeyTemplate;
	}

	public String getPrimaryParamTemplate() {
		return primaryParamTemplate;
	}

	public void setPrimaryParamTemplate(String primaryParamTemplate) {
		this.primaryParamTemplate = primaryParamTemplate;
	}
    
}
