package ${servicePkg};

import ${modelPkg}.${nickName?cap_first};

<#assign gentime = .now> 
/**
 * ${nickName}模块对应的业务接口
 * @since ${gentime?string['yyyy-MM-dd HH:mm']}
 * @author jfast
 */
public interface ${nickName?cap_first}Service{

	/**
	 * 查询${nickName}详情
	 */
	${nickName?cap_first} get${nickName?cap_first}By${primaryKeyTemplate}(${primaryParamTemplate});
	
	/**
	 * 删除${nickName}对象
	 */
	void delete${nickName?cap_first}By${primaryKeyTemplate}(${primaryParamTemplate});
	
	/**
	 * 更新${nickName}对象
	 */
	void update${nickName?cap_first}By${primaryKeyTemplate}(${nickName?cap_first} ${nickName?uncap_first});
	
	/**
	 * 添加${nickName}对象
	 */
	void add${nickName?cap_first}(${nickName?cap_first} ${nickName?uncap_first});

}