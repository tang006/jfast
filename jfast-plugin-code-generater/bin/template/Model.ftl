package ${modelPkg};

<#-- 判断是否需要引入日期包 -->
<#list columns as col>
	<#if col.javaType == "Date">
import java.util.Date;
		<#break>
	</#if>
</#list>
<#-- 判断是否需要引入BigDecimal字段 -->
<#list columns as col>
	<#if col.javaType=="BigDecimal">
import java.math.BigDecimal;
		<#break>
	</#if>
</#list>
<#-- 判断是否需要引入@Table注解 -->
<#if tableName?cap_first != nickName?cap_first>
import cn.jfast.framework.jdbc.annotation.Table;
</#if>
<#assign gentime = .now>
/**
 * ${nickName}模块对应的实体类
 * @since ${gentime?string['yyyy-MM-dd HH:mm']}
 * @author jfast
 */
<#if tableName?cap_first != nickName?cap_first>
@Table("${tableName}")
</#if>
public class ${nickName?cap_first}{

<#list columns as col>
	/** ${col.remarks} */
	private ${col.javaType} ${col.name};	
</#list>
<#list columns as col>
		
	/** ${col.remarks} */
	public void set${col.name?cap_first}(${col.javaType} ${col.name}){
		this.${col.name} = ${col.name};
	}
	
	<#if col.javaType == "Boolean">
	/** ${col.remarks} */
	public Boolean is${col.name?cap_first}(){
		return ${col.name};
	}	
	<#else>
	/** ${col.remarks} */
	public ${col.javaType} get${col.name?cap_first}(){
		return ${col.name};
	}	
	</#if>
</#list>
}