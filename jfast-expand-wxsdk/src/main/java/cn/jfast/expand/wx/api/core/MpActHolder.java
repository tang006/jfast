package cn.jfast.expand.wx.api.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;

import cn.jfast.expand.wx.api.msg.MPAct;
import cn.jfast.framework.base.prop.CoreConsts;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.web.api.ApiContext;

public class MpActHolder {
	
	/** 微信公众号账号缓存 */
	private static Map<String,MPAct> mpActMap = new  ConcurrentHashMap<String, MPAct>();
	private static MpActResource resources;
	private static Logger log = LogFactory.getLogger(LogType.JFast, MpActHolder.class);
	
	static{
		try{
			if(StringUtils.isNotEmpty(CoreConsts.mp_act_resource)){
				resources = (MpActResource) Class.forName(CoreConsts.mp_act_resource).newInstance();
				resources = (MpActResource) ApiContext.fillResource(resources);
				mpActMap.putAll(resources.getResources());
			}
		}catch(ClassNotFoundException e){
			log.error("微信公众号账号缓存资源类无法实例化",e);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized static MPAct getMpAct(String mpKey) {
		return mpActMap.get(mpKey);
	}
	
	public synchronized static void addMPAct(String key,MPAct mpAct){
		mpActMap.put(key, mpAct);
	}

}
