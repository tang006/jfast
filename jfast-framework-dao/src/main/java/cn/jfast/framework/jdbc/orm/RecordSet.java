package cn.jfast.framework.jdbc.orm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RecordSet implements Iterator<Record>{
	
	private List<Record> recordList = new ArrayList<Record>();
	
	private int cur = 0;
	
	@Override
	public boolean hasNext() {
		if(cur < recordList.size())
			return true;
		return false;
	}

	@Override
	public Record next() {
		return recordList.get(cur++);
	}

	@Override
	public void remove() {
		this.recordList.remove(cur);
	}
	
	public void addRecord(Record record){
		this.recordList.add(record);
	}
	
	public <T> List<T> entityList(Class<T> clazz){
		List<T> tList = new ArrayList<T>();
		for(Record record:recordList){
			tList.add(record.entity(clazz));
		}
		return tList;
	}
}
