/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.LogType;
import cn.jfast.framework.log.Logger;

/**
 * Dbcp数据源
 *
 * @author 泛泛o0之辈
 */
public class DbcpDataSource implements IDataSource {

	private BasicDataSource dataSource;
	
	private Logger log = LogFactory.getLogger(LogType.JFast, DbcpDataSource.class);
	/**
	 * 数据源初始化
	 * @return
	 */
	public boolean init(){
		dataSource = new BasicDataSource();
		dataSource.setUrl(DBProp.getJdbcUrl());
		dataSource.setUsername(DBProp.jdbc_user);
		dataSource.setMaxIdle(DBProp.max_idle_time);
		dataSource.setDriverClassName(DBProp.jdbc_driver);
		dataSource.setPassword(DBProp.jdbc_password);
		dataSource.setMaxActive(DBProp.max_pool_size);
		return true;
	}
	
	/**
	 * 获得数据源
	 */
	public DataSource getDataSource() {
		return dataSource;
	}
	
	/**
	 * 关闭数据源
	 * @return
	 */
	public boolean stop() {
		if (dataSource != null)
			try {
				dataSource.close();
			} catch (SQLException e) {
				log.error("", e);
			}
		return true;
	}

}
