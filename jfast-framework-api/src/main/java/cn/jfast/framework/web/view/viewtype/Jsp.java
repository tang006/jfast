/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.base.prop.CoreConsts;
import cn.jfast.framework.web.view.Route;
import cn.jfast.framework.web.view.View;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 返回JSP视图
 * 注意jfast过滤器filter需要过滤forward请求
 * 否则服务端转发jsp页面时,如果静态资源管理配置了
 * jsp转发路径,那么jfast将不能转发到正确路径
 */
public class Jsp extends View {

    private String view;
    private Route route;
    Map<String, Object> requestAttrs = new HashMap<String, Object>();

    public Jsp(String view) {
        this(view, Route.DISPATHER);
    }

    public Jsp(String view, Route route) {
        if (!view.endsWith(".jsp"))
            view = view + ".jsp";
        this.view = view;
        this.route = route;
    }

    public Map<String, Object> getRequestAttrs() {
        return requestAttrs;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Jsp addRedirectAttributes(Map<String, String> attrs) {
        if (!attrs.isEmpty()) {
            this.view += "?";
            for (Map.Entry<String, String> attr : attrs.entrySet()) {
                this.view += "&" + attr.getKey() + "=" + attr.getValue();
            }
            this.view = this.view.replaceFirst("&", "");
        }
        return this;
    }

    public Jsp addDispatherAttribute(String attrName, Object attrValue) {
        requestAttrs.put(attrName, attrValue);
        return this;
    }

    public Jsp addDispatherAttributes(Map<String, String> attrs) {
        requestAttrs.putAll(attrs);
        return this;
    }

    public Jsp addRedirectAttribute(String attrName, String attrValue) {
        try {
            if (this.view.indexOf("?") == -1)
                this.view += "?"
                        + attrName
                        + "="
                        + URLEncoder.encode(attrValue,
                        CoreConsts.encoding);
            else
                this.view += "&"
                        + attrName
                        + "="
                        + URLEncoder.encode(attrValue,
                        CoreConsts.encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public String toString() {
        return "jsp:" + getView();
    }
}
