/**
 * 
 */
package cn.jfast.plugin.generater;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;

/**
 * 打印日志信息
 * @author jfast
 * 2015年8月17日
 */
public class JfastConsole {
	private static MessageConsole jfast ; 
	static{
		jfast = new MessageConsole ("JFast",null);
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{jfast});
	}
	public static void log(String msg){
		jfast.newMessageStream().println(msg);	 
	}
	 
}
